[[Clause_Migration]]
== Migration

GeoPackage 1.2.1 and 1.3.0 are almost identical.
To migrate a 1.2.1 (or 1.2.0) GeoPackage to 1.3.0, execute the following SQLite commands:

```
pragma user_version=10300;
drop trigger gpkg_metadata_md_scope_insert;
drop trigger gpkg_metadata_md_scope_update;
drop trigger gpkg_metadata_reference_reference_scope_insert;
drop trigger gpkg_metadata_reference_reference_scope_update;
drop trigger gpkg_metadata_reference_column_name_insert;
drop trigger gpkg_metadata_reference_column_name_update;
drop trigger gpkg_metadata_reference_row_id_value_insert;
drop trigger gpkg_metadata_reference_row_id_value_update;
drop trigger gpkg_metadata_reference_timestamp_insert;
drop trigger gpkg_metadata_reference_timestamp_update;
```

The first command updates the internal version identifier of the GeoPackage to 1.3.0.
The `drop` commands will drop the Metadata Extension triggers that were removed.
Note that these triggers will not be present unless the Metadata Extension is in use.

